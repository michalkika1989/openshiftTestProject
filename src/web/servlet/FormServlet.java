package web.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;

/**
 * Servlet implementation class FormServlet
 */
@WebServlet("/FormServlet")
@MultipartConfig
public class FormServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FormServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
//		String fname = request.getParameter("fname");
//		String lname = request.getParameter("lname");
//		System.out.println("Name is " + fname + " " + lname);
		
		boolean isMultipart = ServletFileUpload.isMultipartContent(request);
		
		Collection<Part> part = request.getParts();
		
		for(Part item : part){
			if(item.getContentType() != null){				
				System.out.println(item.getName() + " : " + getContent(item.getInputStream()));		
				
			}else{
				System.out.println(item.getName() + " : " + getContent(item.getInputStream()));
			}
		}
		
		response.getWriter().write("File uploaded");
		
	}
	
	private String getContent(InputStream data){
		StringBuilder result = new StringBuilder();
		if(data != null){
			BufferedReader br = new BufferedReader(new InputStreamReader(data));
			String line;
			
			try {
				while((line = br.readLine()) != null){
					result.append(line);
				}
			} catch (IOException e) {
				System.out.println("Not possible read source");
			}
		}
		
		return result.toString();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
